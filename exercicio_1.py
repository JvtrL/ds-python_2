pessoa = None

class Homem(object):
    def __init__(self):
        self.pessoa = "Foi criado um objeto: Homem"

    def registrar(self):
        return self.pessoa

class Mulher(object):
    def __init__(self):
        self.pessoa = "Foi criado um objeto: Mulher"

    def registrar2(self):
        return self.pessoa

escola = int(input("Digite '1' para Homem e '2' para Mulher: "))

if escola == 1:
    print(Homem().registrar())
elif escola == 2:
    print(Mulher().registrar2())
else:
    print("Opção invalida, tente novamente!")