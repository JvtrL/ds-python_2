class Conta(object):
    def __init__(self, numero_da_conta, nome_do_correntista, saldo):
        self.numero_da_conta = numero_da_conta
        self.nome = nome_do_correntista
        self.saldo = saldo

    def alterar_nome(self, novo_nome):
        self.nome = novo_nome
        return self.nome

    def depositar(self, valor):
        self.saldo += valor
        return self.saldo

    def sacar(self, valor):
        if (self.saldo - valor) >= 0:
            self.saldo -= valor
            return self.saldo

    def ver_saldo(self):
        return self.saldo


conta = Conta("12345", "Wesley", 100.0)

while True:
    print('1. alterar nome do correntista')
    print('2. depositar')
    print('3. sacar')
    print('4. Ver saldo da conta')
    print('5. Sair')

    escolha_Operacao = int(input("Digite a operação: "))

    if escolha_Operacao == 1:
        nome_correntista = input("Digite o nome a ser alterado: ")
        print("Nome foi alterado para: ", conta.alterar_nome(nome_correntista))
    elif escolha_Operacao == 2:
        valor_deposito = float(input("Digite o valor à ser depositado: "))
        conta.depositar(valor_deposito)
    elif escolha_Operacao == 3:
        valor_saque = float(input("Digite o valor à ser sacado: "))
        conta.sacar(valor_saque)
    elif escolha_Operacao == 4:
        print("Saldo:", conta.ver_saldo())
    else:
        break
