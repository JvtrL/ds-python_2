# --------------------------Exercicio 5 -----------------------------
class Veiculo(object):
    def __init__(self, marca, modelo, preco):
        self.marca = marca
        self.modelo = modelo
        self.preco = preco

    # ------- Função comprar veiculos -------
    def comprarVeiculo(self):
        return f"Veículo comprado: {self.marca} {self.modelo} por R${self.preco}"

    #------- Função cadastrar veiculos -------
    def cadastrarVeiculo(self, marca, modelo, preco):
        self.marca = marca
        self.modelo = modelo
        self.preco = preco

    #------- Função listagem de veiculos -------
    def listarVeiculos(self):
        return f"Veículo: {self.marca} {self.modelo}, Preço: R${self.preco}"

# ------------- Adicionando Veiculo Inicial -------------
func_veiculo = Veiculo('Mercedes', 'Coupe', 336900.00)

#------- Loop da loja -------
while True:
    print('1. Comprar')
    print('2. Cadastar')
    print('3. Listar')
    print('4. Sair')

    escolha_Operacao = int(input("Digite a opcao: "))

    if escolha_Operacao == 1:
        print(func_veiculo.comprarVeiculo())
    elif escolha_Operacao == 2:
        marca = input("Digite a marca: ")
        modelo = input("Digite o modelo: ")
        preco = float(input("Digite o preço: "))
        func_veiculo.cadastrarVeiculo(marca, modelo, preco)
        print("Veículo cadastrado com sucesso!")
    elif escolha_Operacao == 3:
        print(func_veiculo.listarVeiculos())
    else:
        break
