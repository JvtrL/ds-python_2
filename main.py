# class Aluno(object):
#
#     def __init__(self, nome, idade): # self referencia a classe
#         self.nome = nome             # a variavel é um parametro
#         self.idade = idade
#         self.nota_1 = 0.0
#         self.nota_2 = 0.0
#
#     def addNotas(self, nota1, nota2):
#         self.nota_1 = nota1
#         self.nota_2 = nota2
#
#     def media(self):
#         return (self.nota_1 + self.nota_2) / 2

# aluno_1 = Aluno("Anderson", 23) # instanciando a classe
# print(f"{aluno_1.nome} tem {aluno_1.idade} anos de idade")

# aluno_2 = Aluno("Bruno", 23)
# aluno_2.addNotas(51, 87)
# print(f"{aluno_2.nome} tem {aluno_2.idade} anos de idade e sua media na prova foi de: {aluno_2.media()} pts")

# print(dir(Aluno))

# class Animal(object):
#     pass
#
# class Ave(Animal):
#     pass
#
# class BeijaFlor(Ave):
#     pass

# class Pai(object):
#     nome = "Carlos"
#     sobrenome = "Oliveira"
#     residencia = "Santa Rita"
#     olhos = "azuis"
#
# class Filha(Pai):
#     nome = "Luciana"
#     olhos = "castanhos"
#
# class Neta(Filha):
#     nome = "Maria"
#
# print(Pai.nome, Filha.nome, Neta.nome)
# print(Pai.residencia, Filha.residencia, Neta.residencia)
# print(Pai.olhos, Filha.olhos, Neta.olhos)

# class Pessoa(object):
#     nome = None
#     idade = None
#
#     def __init__(self, nome, idade):
#         self.nome = nome
#         self.idade = idade
#
#     def envelecer(self):
#         self.idade += 1
#
# class Atleta(Pessoa):
#     peso = None
#     aposentado = None
#
#     def __init__(self, nome, idade, peso):
#         super().__init__(nome, idade)
#         self.peso = peso
#
#     def aquecer(self):
#         print("Atleta aquecido!")
#
#     def aposentar(self):
#         self.aposentado = True
#
# class Corredor(Atleta):
#     def correr(self):
#         print("Correndo!")
#
# class Nadador(Atleta):
#     def nadar(self ):
#         print("nadando!")
#
# class Ciclista(Atleta):
#     def pedalar(self ):
#         print("pedalando!")
#
# corredor_1 = Corredor("leandro", 25, 80)
# nadador_1 = Nadador("<Joaquim>", 30, 75)
# ciclista_1 = Ciclista("<Robesvaldo>", 21, 60)
#
# corredor_1.aquecer()
# corredor_1.correr()

# def decorator(func):
#     def wrapper():
#         print("Antes de executar")
#         func()
#         print("Depois de executar")
#     return wrapper
#
# @decorator
# def media_da_turma():
#     nota_1 = 10
#     nota_2 = 5
#     print(f"Media = {(nota_1 + nota_2)/2}")
#
# media_da_turma()

from classes import CalculoImposto

calc = CalculoImposto()
calc.taxa = 55.25

print(calc.taxa)

calc.taxa = 105.6
print(calc.taxa)