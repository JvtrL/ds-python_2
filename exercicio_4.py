class ControlarDesconto(object):
    def __init__(self):
        self.__preco_produto = 0
        self.__porcentagem_desconto = 0

    @property
    def desconto(self):
        self.__preco_produto = self.__preco_produto - (self.__preco_produto * (self.__porcentagem_desconto / 100))

    @desconto.setter
    def desconto(self, val_desconto):
        if val_desconto < 1:
            __porcentagem_desconto = 1
        elif val_desconto > 35:
            __porcentagem_desconto = 1
        else:
            __porcentagem_desconto = val_desconto

    @property
    def preco_produto(self):
        return self.__preco_produto

    @preco_produto.setter
    def preco_produto(self, preco_doProduto):
        if preco_doProduto >= 0:
            self.__preco_produto = preco_doProduto
        else:
            print("Por favor, entre apenas com valores positivos!")


func_desc = ControlarDesconto()

func_desc.preco_produto = 50
func_desc.__porcentagem_desconto = 5
print(func_desc.desconto)