class CalculoImposto(object):
    def __init__(self):
        self._taxa = 0

    @property
    def taxa(self):
        return round(self._taxa, 2)

    @taxa.setter
    def taxa(self, val_taxa):
        if val_taxa < 0:
            self._taxa = 0
        elif val_taxa > 100:
            self._taxa = 100
        else:
            self._taxa = val_taxa