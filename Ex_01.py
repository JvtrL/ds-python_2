class Aluno(object):
    def __init__(self, nome, matricula):
        self.nome = nome
        self.matricula = matricula

    def adicionarNotas_alunos(self, P1, P2):
        self.P1 = P1
        self.P2 = P2
        self.nota_media = ((self.P1 + self.P2) / 2)

    def aluno_aprovado(self):
        if self.nota_media >= 60:
            return "Aprovado"
        elif self.nota_media >= 30:
            return "NP3"
        else:
            return "Reprovado"


aluno_01 = Aluno("Bruno", 123)
aluno_01.adicionarNotas_alunos(51, 87)
print(aluno_01.aluno_aprovado())

aluno_02 = Aluno("Gabriel", 456)
aluno_02.adicionarNotas_alunos(50, 11)
print(aluno_02.aluno_aprovado())